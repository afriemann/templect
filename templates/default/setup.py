
import os, pip

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def read_readme():
    return read('README.rst')

def read_requirements():
    install_reqs = pip.req.parse_requirements('requirements.txt', session=pip.download.PipSession())
    return [str(ir.req) for ir in install_reqs if ir is not None]

setup(name             = "<NAME>",
      author           = "<AUTHOR>",
      author_email     = "<AUTHOR_MAIL>",
      license          = "<LICENSE>",
      version          = "<VERSION>",
      description      = "<DESCRIPTION>",
      keywords         = [<KEYWORDS>],
      classifiers      = [],
      # url              = "<URL>",
      # download_url     = "<DOWNLOAD_URL>",
      install_requires = read_requirements(),
      long_description = read_readme(),
      packages         = find_packages()
)
