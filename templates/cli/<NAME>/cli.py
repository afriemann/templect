#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: TODO
   :synopsis: TODO.

.. moduleauthor:: <AUTHOR> <<AUTHOR_MAIL>>

"""

import click, logging

log = logging.getLogger(__name__)

@click.command(context_settings={'help_option_names':['-h','--help']})
def cli():
    """TODO: Docstring for <NAME>."""
    pass

def run():
    cli()

if __name__ == '__main__':
    run()

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
